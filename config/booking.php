<?php

return [
    'ibis' => [
        'api_url' => env('IBIS_API_URL'),
        'api_key' => env('IBIS_API_KEY'),
    ],

    'fareharbour' => [
        'api_url' => env('FAREHARBOUR_API_URL'),
        'api_key' => env('FAREHARBOUR_API_KEY'),
    ],

    'junction6' => [
        'api_url' => env('JUNCTION6_API_URL'),
    ],

    'resbook' => [
        'api_url' => env('RESBOOK_API_URL'),
    ],

    'rezdy' => [
        'api_url' => env('REZDY_API_URL'),
    ],

    'seekom' => [
        'api_url' => env('SEEKOM_API_URL'),
        'client_id' => env('SEEKOM_CLIENT_ID'),
        'client_secret' => env('SEEKOM_CLIENT_SECRET'),
        'oauth_url' => env('SEEKOM_OAUTH_URL')
    ],

    'bookeo' => [
        'api_url' => env('BOOKEO_API_URL'),
        'application_id' => env('BOOKEO_APPLICATION_ID'),
        'client_secret' => env('BOOKEO_CLIENT_SECRET'),
        'authorization_url' => env('BOOKEO_AUTHORIZATION_URL')
    ],

    'rtbs' => [
        'api_url' => env('RTBS_API_URL'),
        'api_key' => env('RTBS_API_KEY'),
    ],

    'roller' => [
        'api_url' => "https://api.roller.app",
    ],

    'customlinc' => [
        'api_url' => env('CUSTOMLINC_URL'),
    ],

    'crawler' => [
        "api_url" => 'https://crawler.dev.yonderhq.com',
        "api_username" => 'broadcaster',
        "api_password" => 'Pr0T3cTeDP@ssWord'
    ],


];
