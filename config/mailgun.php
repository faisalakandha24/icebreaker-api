<?php

return [
    'api' => [
        'key' => env('MAILGUN_SECRET'),
        'host' => env('MAILGUN_DOMAIN')
    ],
];
