<?php

return [
    "api_url" => env('PAYMENTS_API_URL'),
    "stripe_dashboard_url" => env('STRIPE_DASHBOARD'),
    "stripe_api_key" => env('STRIPE_KEY'),
    "tax_id" => env('TAX_ID'),
    "stripe_secret_key" => env('STRIPE_SECRET')
];
