<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDislikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dislikes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id_one')->unsigned();
            $table->bigInteger('user_id_disliked')->unsigned();
            $table->integer('frequency');
            $table->timestamps();

            $table->foreign('user_id_one')->references('id')->on('users');
            $table->foreign('user_id_disliked')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dislikes');
    }
}
