<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;


class GetLocationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rating' => 'nullable|int',
            'comment' => 'string',
        ];
    }
}
