<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string',
            'name' => 'required|string',
            'date_of_birth' => 'required|integer',
            'sex' => 'required|string',
            'location_name' => 'required|string',
            'longitude' => 'required|integer',
            'latitude' => 'required|integer',
            'password' => 'required|string',
        ];
    }
}
