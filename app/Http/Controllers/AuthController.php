<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\ResetPasswordRequest;
use App\Http\Requests\API\SignupRequest;
use App\Repositories\AuthRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $authRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct($authRepository = null)
    {
        if ($authRepository === null) {
            $authRepository = new AuthRepository();
        }

        $this->authRepository = $authRepository;
    }

    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);

        return $this->authRepository->login($credentials);
    }

    public function signup(SignupRequest $request)
    {
        $userInfo = $request->all();

        unset($userInfo['q']);

        return $this->authRepository->signup($userInfo);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        return $this->authRepository->resetPassword();
    }

    public function me()
    {
        return $this->authRepository->me(auth()->user()->id);
    }
}
