<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\ReportRequest;
use App\Repositories\ActionRepository;
use Illuminate\Routing\Controller as BaseController;

class ActionController extends BaseController
{
    protected $actionRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct($actionRepository = null)
    {
        if ($actionRepository === null) {
            $actionRepository = new ActionRepository();
        }

        $this->actionRepository = $actionRepository;
    }

    public function like($userId)
    {
        return $this->actionRepository->like($userId);
    }

    public function likeGroup($groupId)
    {
        return $this->actionRepository->likeGroup($groupId);
    }

    public function dislike($userId)
    {
        return $this->actionRepository->dislike($userId);
    }

    public function DislikeGroup($groupId)
    {
        return $this->actionRepository->DislikeGroup($groupId);
    }

    public function unmatch($userId)
    {
        return $this->actionRepository->unmatch($userId);
    }

    public function unmatchGroup($groupId)
    {
        return $this->actionRepository->unmatchGroup($groupId);
    }

    public function report(ReportRequest $request, $userId)
    {
        return $this->actionRepository->report($userId, $request->get('reason'));
    }

    public function reportGroup(ReportRequest $request, $groupId)
    {
        return $this->actionRepository->reportGroup($groupId, $request->get('reason'));
    }

    public function findNewVideoSpeedDateWithInProximity()
    {
        return $this->actionRepository->findNewVideoSpeedDateWithInProximity(auth()->user()->id);
    }

    public function findNewVideoGroupChatWithInProximity()
    {
        return $this->actionRepository->findNewVideoGroupChatWithInProximity(auth()->user()->id);
    }

}
