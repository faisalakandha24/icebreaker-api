<?php

namespace App\Repositories;

use App\Events\StartVideoChat;
use App\Models\GroupLobby;
use App\Models\GroupMutualLike;
use App\Models\Like;
use App\Models\ReportUser;
use App\Models\UserLobby;

class ActionRepository
{

    public function unmatch($userId)
    {

    }

    public function like($userId)
    {

    }

    public function report($userId, $reason)
    {
        ReportUser::updateOrCreate(['reporting_user_id' => auth()->user()->id, 'reported_user_id' => $userId], ['reporting_user_id' => auth()->user()->id, 'reported_user_id' => $userId, 'reason' => $reason]);
    }

    public function reportGroup($groupId, $reason)
    {
        ReportUser::updateOrCreate(['reporting_user_id' => auth()->user()->id, 'reported_user_id' => $groupId], ['reporting_user_id' => auth()->user()->id, 'reported_user_id' => $groupId, 'reason' => $reason]);
    }

    public function unmatchGroup($groupId)
    {
        $user = auth()->user();
        GroupMutualLike::where('group_id_one', $user->group_id);
    }

    public function likeGroup($groupId)
    {

    }

    public function dislike($groupId)
    {
    }

    public function dislikeGroup($groupId)
    {
    }

    public function findNewVideoSpeedDateWithInProximity($signalData)
    {
        $doesEntryExists = UserLobby::where('user_id', auth()->user()->id)->first();
        if ($doesEntryExists) {
            return;
        }

        UserLobby::create(['user_id' => auth()->user()->id, 'signal_data' => $signalData]);

        return;
    }

    public function findNewVideoGroupChatWithInProximity($signalData)
    {
        $doesEntryExists = GroupLobby::where('group_id', auth()->user()->group_id)->where('user_id', auth()->user()->id)->first();

        if ($doesEntryExists) {
            GroupLobby::where('group_id', auth()->user()->group_id)->update(['status' => 'searching']);
        }

        GroupLobby::create(['group_id' => auth()->user()->group_id, 'user_id' => auth()->user()->id, 'status' => 'ready', 'signal_data' => $signalData]);

        return;

    }

    public function acceptCall($signal, $to)
    {
        $data['signal'] = $signal;
        $data['to'] = $to;
        $data['type'] = 'callAccepted';
        broadcast(new StartVideoChat($data))->toOthers();
    }
}
