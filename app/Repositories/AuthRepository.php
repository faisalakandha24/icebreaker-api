<?php

namespace App\Repositories;

use App\Models\Group;
use App\Models\Plans;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthRepository
{
    protected $locationRepository;

    protected $profileRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct()
    {
    }

    public function login($credentials)
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
        ]);
    }

    public function signup($userData)
    {
        $user = User::where('email', $userData['email'])->first();

        if ($user != null) {
            return response()->json(['error' => 'User already has an account'], 409);
        }

        $userData['password'] = Hash::make($userData['password']);

        $user = User::create($userData);

        return $this->login(['email' => $userData['email'], 'password' => $userData['password']]);
    }

    public function resetPassword($credentials, $newPassword)
    {

    }

    public function me($userId)
    {
        $user = User::where('id', $userId)->first();
        $user->group = Group::where('user_id_one', $userId)->orWhere('user_id_two', $userId)->orWhere('user_id_three', $userId)->orWhere('user_id_four', $userId)->first();
        $user->plan = Plans::where('id', $user->plan_id)->first();
    }
}
