<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupLobby extends Model
{
    protected $table = 'group_lobby';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status',
    ];

}
