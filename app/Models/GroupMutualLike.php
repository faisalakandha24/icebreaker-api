<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupMutualLike extends Model
{
    protected $table = 'group_mutual_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'group_id_one',
        'group_id_two',
    ];
}
